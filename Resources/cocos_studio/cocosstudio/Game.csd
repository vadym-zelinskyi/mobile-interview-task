<GameFile>
  <PropertyGroup Name="Game" Type="Node" ID="c853bcac-2e69-414e-ae51-4ff362798a06" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="45" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="pnlContainer" ActionTag="1513077061" Tag="13" IconVisible="False" RightMargin="-1280.0000" TopMargin="-800.0000" ClipAble="False" BackColorAlpha="144" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="-337" Scale9OriginY="-337" Scale9Width="674" Scale9Height="674" ctype="PanelObjectData">
            <Size X="1280.0000" Y="800.0000" />
            <Children>
              <AbstractNodeData Name="pnlContainer" ActionTag="314981271" Tag="47" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ClipAble="False" BackColorAlpha="43" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="337" RightEage="337" TopEage="337" BottomEage="337" Scale9OriginX="-337" Scale9OriginY="-337" Scale9Width="674" Scale9Height="674" ctype="PanelObjectData">
                <Size X="1280.0000" Y="800.0000" />
                <Children>
                  <AbstractNodeData Name="imgBackground" ActionTag="1068350643" Tag="24" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="-0.5000" BottomMargin="0.5000" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="1004" Scale9Height="1004" ctype="ImageViewObjectData">
                    <Size X="1280.0000" Y="800.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="640.0000" Y="400.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5006" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/Lava.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="pnlField" ActionTag="1681713147" Tag="34" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="240.0000" RightMargin="240.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="800.0000" Y="800.0000" />
                    <Children>
                      <AbstractNodeData Name="txtResult" ActionTag="678327153" VisibleForFrame="False" Tag="11" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="400.0000" RightMargin="400.0000" TopMargin="400.0000" BottomMargin="400.0000" FontSize="90" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="400.0000" Y="400.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btn_tmp" ActionTag="-1628169338" UserData="1" Tag="35" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="712.0000" BottomMargin="712.0000" TouchEnable="True" FontSize="72" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="88.0000" Y="88.0000" />
                        <AnchorPoint ScaleY="1.0000" />
                        <Position Y="800.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition Y="1.0000" />
                        <PreSize X="0.1100" Y="0.1100" />
                        <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                        <TextColor A="255" R="26" G="26" B="26" />
                        <NormalFileData Type="Normal" Path="Res/btn_tmp.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="640.0000" Y="400.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.6250" Y="1.0000" />
                    <SingleColor A="255" R="191" G="191" B="191" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="leafRight" ActionTag="-1568594909" Tag="25" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="1011.2000" BottomMargin="400.0000" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="381" Scale9Height="463" ctype="ImageViewObjectData">
                    <Size X="268.8000" Y="400.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
                    <Position X="1280.0000" Y="800.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="1.0000" />
                    <PreSize X="0.2100" Y="0.5000" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/Leaf2.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="leafLeft" ActionTag="-1670711980" Tag="27" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="1088.0000" BottomMargin="352.0000" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="303" Scale9Height="508" ctype="ImageViewObjectData">
                    <Size X="192.0000" Y="448.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position Y="800.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="1.0000" />
                    <PreSize X="0.1500" Y="0.5600" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/Leaf1.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="191" G="191" B="191" />
                <FirstColor A="255" R="255" G="0" B="189" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>