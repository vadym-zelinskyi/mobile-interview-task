#include "GameScene.h"

bool GameScene::init()
{
	if (!Super::init())
		return false;

	_mainNode.reset(CSLoader::createNode("Game.csb"));
	if (_mainNode.get() && initGame())
	{
		this->addChild(_mainNode.get());
		return true;
	}
	return false;
}

bool GameScene::initGame()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto pnlCont = _mainNode->getChildByName("pnlContainer");
	if (pnlCont)
	{
		auto pnlSize = pnlCont->getContentSize();
		pnlCont->setScaleX(visibleSize.width / pnlSize.width);
		pnlCont->setScaleY(visibleSize.height / pnlSize.height);

		auto contrlPnl = pnlCont->getChildByName("pnlContainer");
		if (contrlPnl)
		{
			_pnlFileld = contrlPnl->getChildByName("pnlField");
			if (_pnlFileld)
			{
				_cellTmp = (Cell*)_pnlFileld->getChildByName("btn_tmp");
				_pnlFileld->removeChildByName("btn_tmp");

				initField();
				return true;
			}
		}
	}
	return false;
}

void GameScene::initField()
{
	const int bomb = 9;
	auto valid = [this](int i)-> bool {
		return (i >= 0 && i < n);
	};

	auto dist = [this, &valid, &bomb](int i, int j) ->int {
		if (valid(i) && valid(j))
			if (_field[i][j] == bomb)
				return 1;
		return 0;
	};

	for (int i = 0; i < BOMBS; i++)
	{
		vector<int> ji = getDigits(to_string(randomInt(0, 99)));
		if (_field[ji[0]][ji[1]] != bomb)
			_field[ji[0]][ji[1]] = bomb;
		else
			i--;
	}

	int xPos = 0;
	int yPos = _pnlFileld->getContentSize().height;
	float btnSize = yPos / n;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (_field[i][j] != bomb)
				_field[i][j] = dist(i, j - 1) + dist(i, j + 1) + dist(i - 1, j) + dist(i + 1, j) + dist(i + 1, j + 1) + dist(i + 1, j - 1) + dist(i - 1, j - 1) + dist(i - 1, j + 1);

			Cell* cell = (Cell*)_cellTmp->clone();
			CellData* data = new CellData();
			data->i = i;
			data->j = j;
			data->value = _field[i][j];
			cell->setUserData(reinterpret_cast<void*>(data));

			_cells[i][j] = cell;
			cell->setPosition(Vec2(xPos, yPos));
			cell->addClickEventListener(CC_CALLBACK_1(GameScene::onCell, this));
			_pnlFileld->addChild(cell);
			xPos += btnSize;
			//_cellsLeft++;
		}
		xPos = 0;
		yPos -= btnSize;
	}
}

void GameScene::refreshField(Cell* cell)
{
	CellData* useData = reinterpret_cast<CellData*>(cell->getUserData());
	string btnTxt = std::to_string(useData->value);
	if (cell->isEnabled())
	{
		cell->setEnabled(false);
		auto color = getColor(useData->value);
		switch (useData->value)
		{
		case 0:
			btnTxt = "";
			openCells(useData->i, useData->j);
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			cell->setTitleColor(color);;
			break;
		case 9:
			updateGameState(GameState::Lose);
			return;
		default:
			break;
		}

		cell->setTitleText(btnTxt);
	}
}

void GameScene::openCells(int i, int j)
{
	auto valid = [this](int i)-> bool {
		return (i >= 0 && i < n);
	};

	auto openCell = [this, &valid](int ni, int nj) {
		if (valid(ni) && valid(nj))
			refreshField(_cells[ni][nj]);
	};

	openCell(i, j - 1);//TO DO
	openCell(i, j + 1);
	openCell(i - 1, j);
	openCell(i + 1, j);
	openCell(i + 1, j + 1);
	openCell(i + 1, j - 1);
	openCell(i - 1, j - 1);
	openCell(i - 1, j + 1);
}

void GameScene::updateGameState(GameState state)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			auto button = _cells[i][j];
			if (button)
			{
				CellData* useData = reinterpret_cast<CellData*>(button->getUserData());
				string btnTxt = std::to_string(useData->value);
				if (btnTxt == "9")
				{
					button->setTitleText(btnTxt);
					button->setTitleColor(getColor(useData->value));
					if (state == GameState::Lose)
						button->setEnabled(false);
				}
				button->addClickEventListener(nullptr);
			}
		}
	}
}

Color3B GameScene::getColor(int value) const
{
	switch (value)
	{
	case 1: return Color3B::BLUE;
	case 2: return Color3B(0, 153, 51);
	case 3: return Color3B::RED;
	case 4: return Color3B::ORANGE;
	case 5: return Color3B::MAGENTA;
	default: return Color3B::BLACK;
	}
}

void GameScene::onCell(Ref * pSender)
{
	auto cell = dynamic_cast<Cell*>(pSender);
	if (cell)
		refreshField(cell);
}

vector<int> GameScene::getDigits(string data) const
{
	vector<int> ji;
	if (data.length() == 1)
		data = "0" + data;

	for (int j = 0; j < 2; j++)
		ji.push_back(int(data[j] - '0'));
	return ji;
}

int GameScene::randomInt(int min, int max) const
{
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uni(min, max);
	return uni(rng);
}