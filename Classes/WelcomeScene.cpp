#include "WelcomeScene.h"
#include "GameScene.h"


bool WelcomeScene::init()
{
	if (!Super::init())
		return false;

	_mainNode = CSLoader::createNode("Welcome.csb");
	if (_mainNode && initGame())
	{
		this->addChild(_mainNode);
		return true;
	}
	return false;
}

bool WelcomeScene::initGame()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto pnlCont = _mainNode->getChildByName("pnlContainer");
	if (pnlCont)
	{
		auto pnlSize = pnlCont->getContentSize();
		pnlCont->setScaleX(visibleSize.width / pnlSize.width);
		pnlCont->setScaleY(visibleSize.height / pnlSize.height);

		auto contrlPnl = pnlCont->getChildByName("pnlContainer");
		if (contrlPnl)
		{
			auto action = CSLoader::createTimeline("Welcome.csb");
			action->gotoFrameAndPlay(0);
			_mainNode->runAction(action);

			_btnPlay = (ui::Button*)contrlPnl->getChildByName("btnPlay");
			if(_btnPlay)
				_btnPlay->addClickEventListener(CC_CALLBACK_1(WelcomeScene::onPlay, this));
			_btnExit = (ui::Button*)contrlPnl->getChildByName("btnExit");
			return true;
		}
	}
	return false;
}

void WelcomeScene::onPlay(Ref* ref)
{
	auto scene = GameScene::create();
	if (scene)
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene, Color3B(255, 204, 0)));
}
