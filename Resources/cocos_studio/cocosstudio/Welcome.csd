<GameFile>
  <PropertyGroup Name="Welcome" Type="Node" ID="2a59fca1-1f81-4a6c-b2c8-b4b9ac69b1fb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="60" Speed="1.0000">
        <Timeline ActionTag="277370982" Property="Position">
          <PointFrame FrameIndex="0" X="102.1808" Y="540.9661">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="45" X="101.8315" Y="107.0340">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="277370982" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="45" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="277370982" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="45" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-284993769" Property="Position">
          <PointFrame FrameIndex="0" X="1236.6473" Y="581.2314">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="1237.1028" Y="-34.1980">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-284993769" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-284993769" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="541163493" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="Res/Bomb1.png" Plist="Res/ResMap.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Node" Tag="45" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="pnlContainer" ActionTag="1513077061" Tag="13" IconVisible="False" RightMargin="-1280.0000" TopMargin="-800.0000" ClipAble="False" BackColorAlpha="144" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="800.0000" />
            <Children>
              <AbstractNodeData Name="pnlContainer" ActionTag="314981271" Tag="47" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ClipAble="False" BackColorAlpha="150" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1280.0000" Y="800.0000" />
                <Children>
                  <AbstractNodeData Name="imgBackground" ActionTag="1490357665" Tag="48" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="-0.5000" BottomMargin="0.5000" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="1004" Scale9Height="1004" ctype="ImageViewObjectData">
                    <Size X="1280.0000" Y="800.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="640.0000" Y="400.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5006" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/Lava.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="drop1" ActionTag="277370982" Tag="45" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="88.1808" RightMargin="1163.8192" TopMargin="237.5339" BottomMargin="519.4661" ctype="SpriteObjectData">
                    <Size X="28.0000" Y="43.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.1808" Y="540.9661" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0798" Y="0.6762" />
                    <PreSize X="0.0219" Y="0.0538" />
                    <FileData Type="MarkedSubImage" Path="Res/SweatDrop.png" Plist="Res/ResMap.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="leafLeft" ActionTag="-1670651190" Tag="32" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="1088.0000" BottomMargin="352.0000" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="303" Scale9Height="508" ctype="ImageViewObjectData">
                    <Size X="192.0000" Y="448.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position Y="800.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="1.0000" />
                    <PreSize X="0.1500" Y="0.5600" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/Leaf1.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="drop2" ActionTag="-284993769" Tag="46" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1222.6473" RightMargin="29.3527" TopMargin="197.2686" BottomMargin="559.7314" ctype="SpriteObjectData">
                    <Size X="28.0000" Y="43.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="1236.6473" Y="581.2314" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9661" Y="0.7265" />
                    <PreSize X="0.0219" Y="0.0538" />
                    <FileData Type="MarkedSubImage" Path="Res/SweatDrop.png" Plist="Res/ResMap.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="leafRight" ActionTag="758059924" Tag="33" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="1011.2000" BottomMargin="400.0000" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="381" Scale9Height="463" ctype="ImageViewObjectData">
                    <Size X="268.8000" Y="400.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
                    <Position X="1280.0000" Y="800.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="1.0000" />
                    <PreSize X="0.2100" Y="0.5000" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/Leaf2.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="branch" ActionTag="1040254693" Tag="39" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="473.6000" TopMargin="648.8800" BottomMargin="31.1200" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="271" Scale9Height="71" ctype="ImageViewObjectData">
                    <Size X="806.4000" Y="120.0000" />
                    <AnchorPoint />
                    <Position Y="31.1200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.0389" />
                    <PreSize X="0.6300" Y="0.1500" />
                    <FileData Type="MarkedSubImage" Path="Res/scenery/BranchTiny.png" Plist="Res/ResMap.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnPlay" ActionTag="1176037330" Tag="37" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="801.1520" RightMargin="73.8560" TopMargin="430.0800" BottomMargin="237.9200" TouchEnable="True" FontSize="72" ButtonText="Play" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="375" Scale9Height="134" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="404.9920" Y="132.0000" />
                    <AnchorPoint />
                    <Position X="801.1520" Y="237.9200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6259" Y="0.2974" />
                    <PreSize X="0.3164" Y="0.1650" />
                    <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                    <TextColor A="255" R="254" G="218" B="53" />
                    <DisabledFileData Type="MarkedSubImage" Path="Res/menu/ButtonGreen.png" Plist="Res/ResMap.plist" />
                    <PressedFileData Type="MarkedSubImage" Path="Res/menu/ButtonBlue.png" Plist="Res/ResMap.plist" />
                    <NormalFileData Type="MarkedSubImage" Path="Res/menu/ButtonGreen.png" Plist="Res/ResMap.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnAbout" ActionTag="172872493" Tag="38" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="801.1520" RightMargin="73.8560" TopMargin="636.9600" BottomMargin="31.0400" TouchEnable="True" FontSize="72" ButtonText="Exit" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="375" Scale9Height="134" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="404.9920" Y="132.0000" />
                    <AnchorPoint />
                    <Position X="801.1520" Y="31.0400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6259" Y="0.0388" />
                    <PreSize X="0.3164" Y="0.1650" />
                    <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                    <TextColor A="255" R="254" G="218" B="53" />
                    <DisabledFileData Type="MarkedSubImage" Path="Res/menu/ButtonGreen.png" Plist="Res/ResMap.plist" />
                    <PressedFileData Type="MarkedSubImage" Path="Res/menu/ButtonBlue.png" Plist="Res/ResMap.plist" />
                    <NormalFileData Type="MarkedSubImage" Path="Res/menu/ButtonGreen.png" Plist="Res/ResMap.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="blueBlob" ActionTag="1916486610" Tag="40" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1034.2920" RightMargin="102.7080" TopMargin="500.5800" BottomMargin="120.4200" ctype="SpriteObjectData">
                    <Size X="143.0000" Y="179.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="1105.7920" Y="209.9200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8639" Y="0.2624" />
                    <PreSize X="0.1117" Y="0.2237" />
                    <FileData Type="MarkedSubImage" Path="Res/BlueBlob-Normal.png" Plist="Res/ResMap.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bomb" ActionTag="541163493" Tag="21" IconVisible="False" LeftMargin="198.1577" RightMargin="926.8423" TopMargin="474.2314" BottomMargin="110.7686" ctype="SpriteObjectData">
                    <Size X="155.0000" Y="215.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="275.6577" Y="218.2686" />
                    <Scale ScaleX="0.8853" ScaleY="0.8070" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2154" Y="0.2728" />
                    <PreSize X="0.1211" Y="0.2688" />
                    <FileData Type="MarkedSubImage" Path="Res/Bomb1.png" Plist="Res/ResMap.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="redBlob" ActionTag="195663256" Tag="41" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="835.6080" RightMargin="314.3920" TopMargin="305.7800" BottomMargin="347.2200" ctype="SpriteObjectData">
                    <Size X="130.0000" Y="147.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="900.6080" Y="420.7200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7036" Y="0.5259" />
                    <PreSize X="0.1016" Y="0.1838" />
                    <FileData Type="MarkedSubImage" Path="Res/RedBlob-Falling.png" Plist="Res/ResMap.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="OilBlob" ActionTag="473772416" Tag="20" IconVisible="False" LeftMargin="496.0731" RightMargin="550.9269" TopMargin="534.4347" BottomMargin="77.5653" ctype="SpriteObjectData">
                    <Size X="233.0000" Y="188.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="612.5731" Y="171.5653" />
                    <Scale ScaleX="0.7965" ScaleY="0.7477" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4786" Y="0.2145" />
                    <PreSize X="0.1820" Y="0.2350" />
                    <FileData Type="MarkedSubImage" Path="Res/OilBlob.png" Plist="Res/ResMap.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="logo" ActionTag="1802902536" Tag="16" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="49.7258" RightMargin="183.2341" TopMargin="-95.1482" BottomMargin="422.1082" LeftEage="345" RightEage="345" TopEage="156" BottomEage="156" Scale9OriginX="345" Scale9OriginY="156" Scale9Width="357" Scale9Height="161" ctype="ImageViewObjectData">
                    <Size X="1047.0400" Y="473.0400" />
                    <AnchorPoint ScaleX="0.4969" ScaleY="0.5029" />
                    <Position X="570.0000" Y="660.0000" />
                    <Scale ScaleX="0.6207" ScaleY="0.5864" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4453" Y="0.8250" />
                    <PreSize X="0.8180" Y="0.5913" />
                    <FileData Type="Normal" Path="Res/logo.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="255" G="0" B="189" />
                <FirstColor A="255" R="255" G="0" B="189" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>