#pragma once
#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__
#include "preheader.h"

class GameScene : public cocos2d::Scene
{
	typedef cocos2d::Scene Super;
public:
	CREATE_FUNC(GameScene);
	virtual bool init();
	bool initGame();

protected:

	struct CellData
	{
		int value;
		int i;
		int j;
	};

	enum GameState
	{
		Win = 1,
		Lose = 10
	};
	const static int n = 10;
	const int BOMBS = 10;

	void onCell(Ref* pSender);
	void initField();
	void refreshField(Cell* button);
	int randomInt(int min, int max) const;
	vector<int> getDigits(string data) const;
	Color3B getColor(int value) const;
	void openCells(int i, int j);
	void updateGameState(GameState state);

	Node* _pnlFileld;
	Cell* _cellTmp;
	int _field[n][n];
	Cell* _cells[n][n];
	shared_ptr<Node> _mainNode;
};
#endif
