#pragma once
#include "preheader.h"

class WelcomeScene : public cocos2d::Scene
{
	typedef cocos2d::Scene Super;
public:
	CREATE_FUNC(WelcomeScene);
	virtual bool init();
	bool initGame();

protected:
	void onPlay(Ref* ref);
	
	Node* _mainNode;
	Cell* _btnPlay;
	Cell* _btnExit;
};
